# top level app file
require 'sinatra'
require 'json'
require 'haml'

get '/' do
  haml :index, :format => :html5
end

get '/grid' do
  content_type :json

  puts "getting the grid"

  {:some => "grid data"}.to_json
end
